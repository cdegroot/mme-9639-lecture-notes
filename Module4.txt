---
title: MME 9639 Lecture Notes
subtitle: Module 4 - Turbulent Boundary Layers
fontsize: 11pt
geometry: margin=1in
graphics: yes
header-includes:
  - \usepackage{cancel}
---

# Fundamentals of Turbulent Flow

## Introduction

 - Most flows in practical applications are turbulent
 - Turbulence describes motions characterized by irregular fluctuations (mixing, eddy motions) superimposed onto a main flow pattern
 - The fluctuations are so complex in detail that their complete treatment by mathematical means likely impossible
 - The effects of fluctuations are incredibly important; the effect is like an apparent increase in viscosity by a factor of 100-10,000, or even more
 - At large Reynolds numbers there is continuous transport of energy from the main flow into the large eddies
 - Energy is dissipated primarily by small eddies within the boundary layer, very close to the wall
 - For now we consider **fully developed turbulent flow**, meaning we consider the time average of the turbulent motions

## Mean Motion and Fluctuations

 - Let us separate the turbulent flow into its **mean motion** and **fluctuations**, denoting for a generic quantity, $\phi$, the time average as $\overline{\phi}$ and the fluctuation as $\phi^\prime$
 - Let us consider only incompressible flow, so we do not have to consider density and temperature fluctuations
 - The flow variables for incompressible flow are:

$$ u = \overline{u} + u^\prime $$
$$ v = \overline{v} + v^\prime $$
$$ w = \overline{w} + w^\prime $$
$$ p = \overline{p} + p^\prime $$

 - The time average is defined as:

$$ \overline{\phi} = \frac{1}{T} \int_{t_0}^{t_0+T}\phi dt $$

 - where $T$ is the time interval for averaging, which is taken to be sufficiently long to be completely independent of time
 - By definition, the time averages of the fluctuations must be zero over a sufficiently long time interval, i.e.:

$$ \overline{u^\prime} = 0 $$
$$ \overline{v^\prime} = 0 $$
$$ \overline{w^\prime} = 0 $$
$$ \overline{p^\prime} = 0 $$

 - Some other useful rules for time averaging functions $f=f(\mathbf{x}, t)$ and $g=g(\mathbf{x}, t)$:

$$ \overline{\overline{f}} = \overline{f} $$
$$ \overline{f + g} = \overline{f} + \overline{g} $$
$$ \overline{\overline{f}\cdot g} = \overline{f}\cdot \overline{g} $$
$$ \overline{\frac{\partial f}{\partial x}} = \frac{\partial \overline{f}}{\partial x} $$
$$ \overline{\int f dx} = \int \overline{f} dx $$
$$ \overline{f\cdot g} = \overline{f}\overline{g} + \overline{f^\prime g^\prime} $$

## Additional “Apparent” Stresses

 - The presence of fluctuations manifest themselves as an **apparent increase in viscosity**
 - Before deriving the equations of motion, let us consider how fluctuations effect momentum transport
 - Consider a differential element of area $dA$ with normal vector in the x-direction
 - The flux of momentum per unit time through the area is:

$$ d\mathbf{J} = d\mathbf{A}\cdot(\rho\mathbf{u}\mathbf{u}) = dA \rho u \mathbf{u} $$

  - The vector components are then:

$$ dJ_x = dA \rho u^2 $$
$$ dJ_y = dA \rho u v $$
$$ dJ_z = dA \rho u w $$

 - Taking the time averages:

$$ \overline{dJ_x} = dA \rho \overline{u^2} = dA \rho \left( \overline{u}^2 + \overline{u^\prime u^\prime} \right) $$
$$ \overline{dJ_y} = dA \rho \overline{u v} = dA \rho \left( \overline{u}~\overline{v} + \overline{u^\prime v^\prime} \right) $$
$$ \overline{dJ_z} = dA \rho \overline{u w} = dA \rho \left( \overline{u}~\overline{w} + \overline{u^\prime w^\prime} \right) $$

 - Flux of momentum per unit time has units of force
 - Divide by area to get force per unit area, i.e. stress
 - Flux of momentum per unit time through an area is equal in magnitude and opposite in sign to the force exerted on the area by the surroundings
 - Therefore, we define the following stress components (one normal and two shear):

$$ \sigma_x = - \rho \left( \overline{u}^2 + \overline{u^\prime u^\prime} \right) $$
$$ \tau_{xy} = - \rho \left( \overline{u}~\overline{v} + \overline{u^\prime v^\prime} \right) $$
$$ \tau_{xz} = - \rho \left( \overline{u}~\overline{w} + \overline{u^\prime w^\prime} \right) $$

 - The fluctuations have therefore given rise to the additional apparent stresses:

$$ \sigma_x^\prime = - \rho \overline{u^\prime u^\prime} $$
$$ \tau_{xy}^\prime = - \rho \overline{u^\prime v^\prime} $$
$$ \tau_{xz}^\prime = - \rho \overline{u^\prime w^\prime} $$

 - These are called the **Reynolds stresses**
 - Similar expressions can be derived for differential areas with normal vectors in the y- and z-directions for form the full Reynolds stress tensor
 - Consider the term $\overline{u^\prime v^\prime}$ in a boundary layer

\begin{center}
  \includegraphics[width=0.5\textwidth]{Figures/TurbulentFluctuationTransport.jpg}
\end{center}

 - Assume $\partial\overline{u}/\partial y > 0$
 - For positive fluctuation $v^\prime$:
     - Fluid moves into faster stream and is slower than surrounding fluid
     - $u^\prime < 0$
     - $u^\prime v^\prime < 0$
 - For negative fluctuation $-v^\prime$:
     - Fluid moves into slower stream and is faster than surrounding fluid
     - $u^\prime > 0$
     - $u^\prime v^\prime < 0$
 - Shows that $\overline{u^\prime v^\prime} <0$
 - Reynolds stress $\tau_{xy}^\prime = - \rho \overline{u^\prime v^\prime} > 0$ has the same sign as the laminar shearing stress
 - There is a **correlation** between $u^\prime$ and $v^\prime$


# Time-Averaged Navier-Stokes Equations

 - Having shown form of Reynolds stresses, we will formally derive from Navier-Stokes
 - The time-averaged or Reynolds-averaged Navier-Stokes (RANS) equations are derived by decomposing the fields into their mean and fluctuating components, then averaging the equations in time, using the definitions and theorems shown previously
 - The continuity and (incompressible) Navier-Stokes equations are:

$$ \frac{\partial u_j}{\partial x_j} = 0 $$

$$ \rho \left( \frac{\partial u_i}{\partial t}
 + u_j \frac{\partial u_i}{\partial x_j} \right)
 = - \frac{\partial p}{\partial x_i}
 + \mu \frac{\partial^2 u_i}{\partial x_j^2}
$$

 - Decomposing into mean and fluctuating components:

$$ \frac{\partial \overline{u}_j}{\partial x_j}
 + \frac{\partial u_j^\prime}{\partial x_j}
 = 0
$$

$$ \rho \left[ \cancel{\frac{\partial \overline{u}_i}{\partial t}}
 + \frac{\partial u_i^\prime}{\partial t}
 + (\overline{u}_j + u_j^\prime)
 \left(\frac{\partial \overline{u}_i}{\partial x_j} + \frac{\partial u_i^\prime}{\partial x_j} \right) \right]
 = - \frac{\partial \overline{p}}{\partial x_i}
   - \frac{\partial p^\prime}{\partial x_i}
 + \mu \left( \frac{\partial^2 \overline{u}_i}{\partial x_j^2}
 + \frac{\partial^2 u_i^\prime}{\partial x_j^2}\right)
$$

 - Time-averaging (recalling, time average of fluctuations is zero):

$$ \frac{\partial \overline{u}_j}{\partial x_j} = 0 $$

$$ \rho \left( \overline{\overline{u}_j \frac{\partial \overline{u}_i}{\partial x_j}}
 + \overline{\overline{u}_j \frac{\partial u_i^\prime}{\partial x_j}}
 + \overline{u_j^\prime \frac{\partial \overline{u}_i}{\partial x_j}}
 + \overline{u_j^\prime \frac{\partial u_i^\prime}{\partial x_j}}
 \right)
 = - \frac{\partial \overline{p}}{\partial x_i}
 + \mu \frac{\partial^2 \overline{u}_i}{\partial x_j^2}
$$

 - Averaged continuity equation, along with regular continuity equation, implies

$$ \frac{\partial u_j^\prime}{\partial x_j} = 0 $$

 - Averaging terms already in terms of averages can be simplified, i.e.

$$ \rho \left( \overline{u}_j \frac{\partial \overline{u}_i}{\partial x_j}
 + \overline{u}_j \cancel{\frac{\partial \overline{u_i^\prime}}{\partial x_j}}
 + \cancel{\overline{u_j^\prime}} \frac{\partial \overline{u}_i}{\partial x_j}
 + \overline{u_j^\prime \frac{\partial u_i^\prime}{\partial x_j}}
 \right)
 = - \frac{\partial \overline{p}}{\partial x_i}
 + \mu \frac{\partial^2 \overline{u}_i}{\partial x_j^2}
$$

 - Since $\partial u_j^\prime/\partial x_j = 0$:

$$ u_j^\prime \frac{\partial u_i^\prime}{\partial x_j} =
   \frac{\partial(u_i^\prime u_j^\prime)}{\partial x_j}
$$

 - Therefore, RANS equations are:

$$ \frac{\partial \overline{u}_j}{\partial x_j} = 0 $$

$$ \rho \overline{u}_j \frac{\partial \overline{u}_i}{\partial x_j}
 = - \frac{\partial \overline{p}}{\partial x_i}
 + \mu \frac{\partial^2 \overline{u}_i}{\partial x_j^2}
 - \rho \frac{\partial \overline{u_i^\prime u_j^\prime}}{\partial x_j}
$$

 - Written in full (2D):

$$ \frac{\partial \overline{u}}{\partial x}
 + \frac{\partial \overline{v}}{\partial y}
 = 0
$$

$$ \rho \left(\overline{u} \frac{\partial \overline{u}}{\partial x}
 + \overline{v} \frac{\partial \overline{u}}{\partial y} \right)
 = - \frac{\partial \overline{p}}{\partial x}
 + \mu \nabla^2 \overline{u}
 - \rho \left( \frac{\partial \overline{u^\prime u^\prime}}{\partial x}
 + \frac{\partial \overline{u^\prime v^\prime}}{\partial y} \right)
$$

$$ \rho \left(\overline{u} \frac{\partial \overline{v}}{\partial x}
 + \overline{v} \frac{\partial \overline{v}}{\partial y} \right)
 = - \frac{\partial \overline{p}}{\partial y}
 + \mu \nabla^2 \overline{v}
 - \rho \left( \frac{\partial \overline{u^\prime v^\prime}}{\partial x}
 + \frac{\partial \overline{v^\prime v^\prime}}{\partial y} \right)
$$

 - The left side, plus pressure and viscous term are identical to laminar case, except with time-averaged values replacing usual values
 - Additional terms have form of the Reynolds stresses discussed previously
 - In 3D the additional stress tensor is:

$$ \mathbf{\sigma}^\prime = -
  \left(\begin{matrix}
  \rho \overline{u^\prime u^\prime} &
  \rho \overline{u^\prime v^\prime} &
  \rho \overline{u^\prime w^\prime} \\
  \rho \overline{u^\prime v^\prime} &
  \rho \overline{v^\prime v^\prime} &
  \rho \overline{v^\prime w^\prime} \\
  \rho \overline{u^\prime w^\prime} &
  \rho \overline{v^\prime w^\prime} &
  \rho \overline{w^\prime w^\prime} \\
 \end{matrix}\right)
$$

 - Or in indicial notation:

$$ \sigma_{ij}^\prime = -\rho \overline{u_i^\prime u_j^\prime} $$

 - Then the momentum equation is:

$$ \rho \overline{u}_j \frac{\partial \overline{u}_i}{\partial x_j}
 = - \frac{\partial \overline{p}}{\partial x_i}
 + \mu \frac{\partial^2 \overline{u}_i}{\partial x_j^2}
 + \frac{\partial \sigma_{ij}}{\partial x_j}
$$

 - The boundary conditions are the same as for laminar flow (i.e. no slip at walls)
 - In most of flow, turbulent stresses are much larger than laminar stresses
 - Near the wall, turbulent fluctuations must vanish, giving rise to very small region near the wall (called **laminar sublayer** or **viscous sublayer**) where laminar stresses dominate
 - In this region near the wall there is no turbulence

# Boussinesq Eddy Viscosity Assumption

 - Form of Reynolds stresses so far not useful for calculating mean flow because they are in terms of fluctuations
 - Laminar shear stress is

$$ \tau_l = \mu \frac{\partial u}{\partial y} $$

 - By analogy, Boussinesq proposed

$$ \tau_t =
  -\rho \overline{u^\prime v^\prime} =
  A_\tau \frac{\partial\overline{u}}{\partial y}
$$

 - The turbulent mixing coefficient, $A_\tau$, corresponds to viscosity and is therefore often called the **apparent viscosity** or **eddy viscosity**
 - The kinematic eddy viscosity is defined as $\epsilon = A_\tau/\rho$
 - The disadvantage is that $A_\tau$ is not a property of the fluid like $\mu$, but depends on mean flow velocity, $\overline{u}$


# Prandtl’s Mixing Length Theory

 - Boussinesq’s assumption helps to define the Reynolds stresses in terms of the mean flow, but if nothing is known about the dependence of $A_\tau$ on the mean flow, it can still not be used for calculations
 - Prandtl proposed a theory based on the simple case of a parallel flow where the velocity varies only from streamline to streamline, but not along the streamlines
 - The velocity is given as:

$$ \overline{u} = \overline{u}(y) $$
$$ \overline{v} = 0 $$
$$ \overline{w} = 0 $$

 - Assume fluid particles coalesce into lumps which move together as bodies and cling together for a given length, retaining their x-momentum
 - Consider the following flow on different streamlines:

\begin{center}
  \includegraphics[width=0.5\textwidth]{Figures/PrandtlMixingLength.jpg}
\end{center}

 - Assume that a parcel of fluid arrives at $y_1$ from a layer at ($y_1-\ell$) and has velocity $\overline{u}(y_1-\ell)$
 - The fluid has been displaced by a distance $\ell$ which is known as **Prandtl’s mixing length**
 - If the parcel of fluid retains its original momentum, its velocity in the new lamina is smaller than the prevailing velocity
 - The difference in velocities is:

$$ \Delta u_1 = \overline{u}(y_1) - \overline{u}(y_1-\ell)
 \approx \ell \left( \frac{\partial \overline{u}}{\partial y} \right)_1
$$

 - For this transverse motion, $v^\prime>0$
 - For a similar parcel of fluid arriving at $y_1$ from $(y_1+\ell)$:

$$ \Delta u_2 = \overline{u}(y_1 + \ell) - \overline{u}(y_1)
 \approx \ell \left( \frac{\partial \overline{u}}{\partial y} \right)_1
$$

 - In this case $v^\prime<0$
 - The velocity differences can be regarded as the turbulent velocity fluctuations at $y_1$

$$ \overline{\left| u^\prime \right|}
 = \frac{1}{2}\left( \left| \Delta u_1 \right| + \left| \Delta u_2 \right| \right)
 = \ell \left| \left(\frac{\partial \overline{u}}{\partial y}\right)_1 \right|
$$

 - Mixing length, $\ell$, is interpreted as distance in transverse direction which must be covered by the fluid particles travelling with their original mean velocity such that the difference between its velocity and the velocity in the new lamina is similar to the mean transverse fluctuation
 - Presume average magnitudes of $u^\prime$ and $v^\prime$ are the same:

$$ \overline{\left| v^\prime \right|} =
 \text{const} \cdot \overline{\left| u^\prime \right|} =
 \text{const} \cdot \ell \left| \frac{\partial \overline{u}}{\partial y} \right|
$$

 - For a fluid parcel arriving at layer $y_1$ with positive $v^\prime$ generally results in negative $u^\prime$
 - Similarly, for a fluid parcel arriving at layer $y_1$ with negative $v^\prime$ generally results in positive $u^\prime$
 - Therefore product $u^\prime v^\prime$ is expected to be negative
 - Assume:

$$ \overline{u^\prime v^\prime}
 = - c_1 \overline{\left| u^\prime \right|} \cdot \overline{\left| v^\prime \right|}
 = - c_2 \ell^2 \left( \frac{\partial \overline{u}}{\partial y} \right)^2
$$

 - Absorb the constant into the still unknown mixing length:

$$ \overline{u^\prime v^\prime}
 = - \ell^2 \left( \frac{\partial \overline{u}}{\partial y} \right)^2
$$

 - The shear stress is then:

$$ \tau_t = -\rho \overline{u^\prime v^\prime}
 = \rho \ell^2 \left( \frac{\partial \overline{u}}{\partial y} \right)^2
$$

 - Since sign of $\tau_t$ must change sign with $\partial\overline{u}/\partial y$, the final expression is:

$$ \tau_t = \rho \ell^2 \left| \frac{\partial \overline{u}}{\partial y} \right|\frac{\partial \overline{u}}{\partial y}
$$

 - This is **Prandtl’s mixing-length hypothesis**
 - Compare to the Boussinesq eddy viscosity model, given as:

$$ \tau_t = A_\tau \frac{\partial\overline{u}}{\partial y} $$

 - The turbulent viscosity coefficient $A_\tau$, also known as turbulent viscosity $\mu_t$ is then:

$$ \mu_t = \rho \ell^2 \left| \frac{\partial \overline{u}}{\partial y} \right| $$

 - More conveniently, the turbulent kinematic viscosity is:

$$ \nu_t = \ell^2 \left| \frac{\partial \overline{u}}{\partial y} \right| $$


# Equation for Kinetic Energy of Turbulent Fluctuations

 - Balance of kinetic energy very important to understanding turbulent fluctuations
 - Define turbulent kinetic energy:

$$ k = \frac{1}{2}\overline{q^2}
 = \frac{1}{2}\left( \overline{u^{\prime 2} + v^{\prime 2} + w^{\prime 2}} \right)
$$

 - Where $q^2 = u^{\prime 2} + v^{\prime 2} + w^{\prime 2}$
 - Equation for kinetic energy can be derived from Navier-Stokes, and is given as:

$$ \underbrace{\frac{\partial k}{\partial t}
 + \overline{u}_j \frac{\partial k}{\partial x_j}}_{\text{Rate of change}\atop\text{following mean flow}}
 = \underbrace{\frac{\partial}{\partial x_j}\left(-\frac{1}{\rho} \overline{u_j^\prime p^\prime} - \frac{1}{2} \overline{q^2 u_j^\prime} \right)}_{\text{Turbulent diffusion}}
 + \underbrace{\nu \left( \frac{\partial^2 k}{\partial x_j^2} + \frac{\partial^2\overline{u_i^\prime u_j^\prime}}{\partial x_i \partial x_j} \right)}_{\text{Viscous}\atop\text{diffusion}}
 - \underbrace{\overline{u_i^\prime u_j^\prime} \frac{\partial\overline{u}_i}{\partial x_j} }_{\text{Production}}
 - \underbrace{2\nu \overline{S_{ij}^\prime S_{ij}^\prime}}_{\text{Dissipation}}
$$


 - Where the fluctuating strain rate tensor is defined as

$$ S_{ij}^\prime = \frac{1}{2} \left( \frac{\partial u_i^\prime}{\partial x_j} + \frac{\partial u_j^\prime}{\partial x_i} \right) $$

 - The $k$-equation describes a balance between four main contributions to the **energy budget** of the turbulent fluctuations
     - Advection
     - Diffusion (molecular diffusion, pressure diffusion, plus turbulent transport)
     - Production
     - Dissipation
 - Left side represents time rate of change of $k$ following the mean flow
 - Dissipation term is always negative (energy sink)
 - Production term is generally positive
     - Positive production indicates energy transfer from mean flow to fluctuations (increasing kinetic energy of fluctuations)
     - Negative production indicates energy transfer from fluctuations to mean flow (decreasing kinetic energy of fluctuations)
 - When dissipation and production are much larger than remaining terms, it is called an **equilibrium region** since production balances with dissipation


# Couette Flow

 - Fully developed Couette flow is simple plane shear flow with constant shear stress everywhere
 - Will treat turbulent case in detail, which gives some general results important for all flows near a wall
 - Fully-developed turbulent flow between parallel plates spaced $2H$ apart
     - If $\partial\overline{u}/\partial x = 0$ then by continuity $\overline{v}=0$
 - No pressure gradient, so it is a pure shear flow
 - Upper wall velocity twice mean velocity at centreline ($y=H$), i.e. $u_{wu}=2\overline{u}_c$

\begin{center}
  \includegraphics[width=0.7\textwidth]{Figures/TurbulentCouette.jpg}
\end{center}

 - Shear stress, $\overline{\tau}_w$, keeps upper plate in motion and has constant value for all $y$, all of the way to lower plate
 - Mean velocity gradient not constant, so Reynolds stresses must contribute to shear stress balance
 - Balance of forces:

$$ \overline{\tau} = \overline{\tau}_v + \tau_t = \overline{\tau}_w = \text{const} $$

 - Recall:

$$ \overline{\tau}_v = \rho \nu \frac{d\overline{u}}{dy} $$
$$ \tau_t = - \rho \overline{u^\prime v^\prime} $$

 - There are two mechanisms by which the momentum component parallel to the wall can be carried through the flow from one wall to the other
    - First is by molecular diffusion due to viscosity (i.e. $\overline{\tau}_v$)
    - Second is due to turbulent fluctuations (i.e. $\overline{\tau}_t$)
 - Based on the wall shear stress, we introduce a velocity scale called the **friction velocity**:

$$ u_\tau = \sqrt{\frac{\overline{\tau}_w}{\rho}} $$

 - Define the dimensionless quantities:

$$ \eta = \frac{y}{H} $$
$$ u^+ = \frac{\overline{u}}{u_\tau} $$
$$ Re_\tau = \frac{u_\tau H}{\nu} $$
$$ \tau_t^+ = \frac{\tau_t}{\rho u_\tau^2} $$

 - Results in:

$$ \overline{u} = f(y, H, \nu, \overline{\tau}_w/\rho) \Rightarrow u^+ = F(\eta, Re) $$

 - Viscous stress in dimensionless form:

$$ \overline{\tau}_v = \rho \nu \frac{d\overline{u}}{dy} = \rho\nu u_\tau \frac{d\eta}{dy}\frac{du^+}{d\eta} = \frac{\rho\nu u_\tau}{H}\frac{du^+}{d\eta}$$

 - Rewrite equation relating shear stresses in dimensionless form:

$$ \overline{\tau}_v + \overline{\tau}_t = \overline{\tau}_w$$

$$ \frac{\rho\nu u_\tau}{H}\frac{du^+}{d\eta} + \rho u_\tau^2 \tau_t^+ = \rho u_\tau^2 $$

$$ \frac{1}{Re_\tau}\frac{du^+}{d\eta} + \tau_t^+ = 1 $$

 - Results in differential equation for dimensionless velocity in terms of dimensionless turbulent stresses
 - Consider only lower region $0<\eta<1$
 - Boundary conditions:

$$ u^+ = 0 \text{ at } \eta=0 $$
$$ \frac{d^2 u^+}{d\eta^2} = 0 \text{ at } \eta=1 $$

 - Last boundary condition implies a point of inflection at centreline
 - By change of coordinate system we can exchange upper and lower walls; implies anti-symmetric velocity distribution about centreline
 - In the limit $Re_\tau\rightarrow \infty$, $\tau_t^+=1$, representing the core region of the flow
 - In this region momentum transfer due to viscosity can be neglected in comparison to turbulent momentum transfer
 - This is valid almost everywhere in a turbulent flow, except near the walls, since $\tau_t^+ = 0$ at the wall
 - Therefore Couette flow at large $Re_\tau$ has a **two-layer structure**
      - **Core layer** where turbulent momentum transfer dominates
      - **Thin wall layer** where both turbulent and molecular momentum transfers act
 - Core layer has thickness of magnitude $H$
 - Wall layer thickness:

$$ \delta_v = \frac{\nu}{u_\tau} = \frac{H}{Re_\tau} $$

 - Introducing a characteristic (stretched) wall coordinate for the wall layer:

$$ y^+ = \frac{y}{\delta_v} = \frac{y}{H}Re_\tau = \eta Re_\tau $$

 - Differential equation in the wall layer becomes:

$$ \frac{du^+}{dy^+} + \tau_t^+ = 1 $$

 - Boundary conditions at the wall:

$$ u^+ = 0 \text{ at } y^+=0 $$
$$ \frac{d u^+}{dy^+} = 1 \text{ at } y^+=0 $$

 - This will produce solution $u^+=f(y^+)$
 - Solutions in core layer ($\tau_t^+=1$) will have to be matched with wall layer, defining overlap layer
 - In the overlap layer assume:

$$ \frac{d\overline{u}}{dy} = f(y, \overline{\tau}_w/\rho) $$

 - Dimensional analysis gives:

$$ \hat{y} \frac{du^+}{d\hat{y}} = \frac{1}{\kappa} = \text{const} $$

 - where $\hat{y} = \eta Re_\tau^\alpha$ with $0<\alpha<1$ is an **intermediate coordinate** for the overlap layer
 - $\kappa$ is the von Karman constant and $\kappa\approx 0.41$
 - Using this equation to form matching conditions between layers
 - Matching condition for core and overlap layer:

$$ \lim_{\eta\rightarrow 0} \frac{du^+}{d\eta} = \frac{1}{\kappa \eta} $$

- Matching condition for wall and overlap layer:

$$ \lim_{y^+\rightarrow \infty} \frac{du^+}{dy^+} = \frac{1}{\kappa y^+} $$

 - Integrating the last equation gives:

$$ \lim_{y^+\rightarrow \infty} u^+(y^+) = \frac{1}{\kappa}\ln y^+ + C^+ $$

 - This equation is called the **logarithmic overlap law**
 - Describes how universal law of the wall behaves for $y^+\rightarrow \infty$
 - Constant of integration for smooth walls is $C^+ = 5.0$

# Universal Laws of the Wall

 - Velocity distribution in wall layer of Couette flow has a universal character
 - Almost all turbulent flows (in the limit of large $Re_\tau$) at finite wall shear stress exhibit a thin wall layer with same velocity distribution as Couette flow
 - Can define **universal law of the wall**, with the following three regions
     - Pure viscous sublayer; $0 \le y^+ < 5$; $u^+=y^+$
     - Buffer layer; $5 < y^+ < 30$; Blend between viscous sublayer and overlap layer
     - Overlap (logarithmic layer); $y^+ > 30$; $u^+ = \frac{1}{\kappa}\ln y^+ + C^+$

\begin{center}
  \includegraphics[width=0.5\textwidth]{Figures/LawOfTheWall.jpg}
\end{center}

 - There are some complex mathematical formulas for buffer layer velocity; see Schlichting for details

# Energy Balances for Couette Flow

## Energy Balance for Mean Motion

 - Start with differential equation for wall layer:

$$ \frac{du^+}{dy^+} + \tau_t^+ = 1 $$

 - Multiply by $du^+/dy^+$:

$$ \underbrace{\frac{du^+}{dy^+}}_{\text{Energy supply}} =
 \underbrace{\left(\frac{du^+}{dy^+}\right)^2}_{\text{Dissipation}}
+ \underbrace{\frac{du^+}{dy^+} \tau_t^+}_{\text{Production}}
$$

 - Energy supplied by mean flow is in two parts:
      - Direct dissipation into internal energy by viscous dissipations
      - Transfer of energy to generate turbulent fluctuations (to produce turbulence)

\begin{center}
  \includegraphics[width=0.4\textwidth]{Figures/CouetteMeanEnergyBalance.jpg}
\end{center}

## Energy Balance for Turbulent Fluctuations

 - In the wall layer, the kinetic energy equation reduces to balance of production, diffusion, and dissipation

\begin{center}
  \includegraphics[width=0.5\textwidth]{Figures/CouetteFluctuationEnergyBalance.jpg}
\end{center}

 - In the overlap layer, production balances dissipation (i.e. it is an equilibrium layer)

# Influence of Wall Roughness

 - Up to now we assumed walls are smooth
 - In reality walls have some roughness
 - There are an infinite number of possible surface states so **standard roughness** is used to describe effect of roughness on flow
 - Assumed wall is covered in layer of tightly packed spheres, which is closely approximated by sandpaper
 - Standard roughness is also called **sand roughness**
 - Diameter of spheres is called **sand roughness height**, given symbol $k_s$

\begin{center}
  \includegraphics[width=0.5\textwidth]{Figures/SandRoughnessHeight.jpg}
\end{center}

 - Real roughness elements can generally be assigned an **equivalent sand roughness**
 - Therefore, consider effect of sand roughness on law of the wall
 - Dimensionless roughness height, based on thickness of wall layer:

$$ k_s^+ = \frac{k_s}{\delta_v} = \frac{k_s u_\tau}{\nu} $$

 - Constant of integration $C^+$ is a function of **roughness characteristic number**, $k_s^+$
 - For a smooth wall:

$$ \lim_{k_s^+\rightarrow\infty} C^+(k_s^+) = 5.0 $$

 - The dimensionless coordinate can be re-written in terms of sand roughness:

$$ y^+ = \frac{y u_\tau}{\nu} = \frac{y k_s^+}{k_s} $$

 - It follows for the overlap law that:

$$ \lim_{y^+\rightarrow \infty} u^+(y^+) = \frac{1}{\kappa}\ln y^+ + C^+(k_s^+)
 = \frac{1}{\kappa}\ln\left(\frac{y}{k_s}\right) + \frac{1}{\kappa}\ln k_s^+ + C^+(k_s^+)
$$

 - Or:

$$ \lim_{y^+\rightarrow \infty} u^+(y^+)
 = \frac{1}{\kappa}\ln\left(\frac{y}{k_s}\right) + C_r^+(k_s^+)
$$

 - Where:

$$ C_r^+(k_s^+) = \frac{1}{\kappa}\ln k_s^+ + C^+(k_s^+) $$

 - If $k_s$ is very large, i.e. $k_s \gg \delta_v$, then roughness elements cover entire wall layer
 - In this case $C_r^+(k_s^+)$ is a constant
 - From experiments, in the **fully rough regime**:

$$ \lim_{k_s^+\rightarrow\infty} C_r^+(k_s^+) = 8.0 $$

 - For every **technical roughness** element we can assign an **equivalent sand roughness**, $k_{s,eq}$
 - By doing experiment to determine distribution of $u^+(y)$ the equivalent roughness can be found (assuming fully rough condition) by re-arranging the log-layer equation:

$$ k_{s,eq} = \exp\left\{\kappa \lim_{y\rightarrow 0} \left[8.0 + \frac{1}{\kappa}\ln y - u^+(y)\right] \right\} $$

 - Turns out the functions $C^+(k_s^+)$ and $C_r^+(k_s^+)$ for technical roughness are different from those for sand roughness
 - For technical roughness:

$$ C^+(k_{tech}^+) = 8.0 - \frac{1}{\kappa}\ln \left(3.4 + k_{tech}^+ \right)
 = 5.0 - \frac{1}{\kappa}\ln \left(1 + \frac{k_{tech}^+}{3.4} \right)
$$

 - Similar expression can be derived for $C_r^+(k_{tech}^+)$

\begin{center}
  \includegraphics[width=0.5\textwidth]{Figures/LogLayerRoughnessConstant.jpg}
\end{center}

 - There are generally three regimes for roughness
     - Hydraulically smooth; $0\le k_s^+ \le 5$; $C^+\approx 5.0$
     - Transition region; $5 < k_s^+ < 70$; $C^+(k_s^+)$
     - Fully rough; $70 \le k_s^+$; $C_r^+\approx 8.0$

 - If roughness elements are completely within viscous sublayer $k_s  < 5\delta_v$ there is no effect on the turbulence and therefore no difference from smooth wall case
 - If roughness elements project out beyond viscous layer they effect the flow, usually increasing drag
 - Some roughness elements can damp turbulent fluctuations and reduce drag
 - Roughness elements in the form of “riblets” with width and depth around $15\delta_v$ have been shown to achieve up to 8% reduction in friction drag

# Turbulence Models

## Introduction

 - We will consider turbulence models for plane flows with constant physical properties
 - RANS equations (or boundary layer RANS equations) do not have a closed form
 - A **turbulence model** is required to connect the turbulent stress $\tau_t$ with the mean motion
 - In general turbulence model is partial differential equation, but may be ordinary differential equation, or algebraic equation
 - Sometimes PDE introduces new unknowns, requiring additional PDEs to model those unknowns
 - Depending on how many PDEs are used, models are called **one-equation models**, **two-equation models**, etc.
 - If an ODE is introduced to a one-equation model, then it is called a **one-and-a-half-equation** model
 - An **algebraic turbulence model** or **zero-equation model** give direct expression between $\tau_t$ and the mean motion
 - At high $Re$, turbulent boundary layers have layered structure of **viscous wall layer** and **fully turbulent outer flow**
 - At finite wall shear stress $\overline{\tau}_w$ the thickness of the wall layer is proportional to $\delta_v=\nu/u_\tau$
 - In limit $Re\rightarrow\infty$, viscous layer is so thin that inertial and pressure forces can be neglected in comparison to friction forces
 - Therefore, the viscous wall layer is identical to Couette flow solution with same $\overline{\tau}_w$ and $\nu$
 - To compute turbulent flows at large $Re$ we do not need detailed description of flow in wall layer
 - Simply compute fully turbulent outer flow and match with wall boundary conditions based on log law:

$$ \lim_{y\rightarrow 0} \overline{u}
 = u_\tau(x)\left[ \frac{1}{\kappa}\ln\left(\frac{y u_\tau(x)}{\nu}\right) + C^+\right]
$$

$$ \lim_{y\rightarrow 0} \frac{\partial\overline{u}}{\partial y}
 = \frac{u_\tau(x)}{\kappa y}
$$

$$ \lim_{y\rightarrow 0} \overline{v} = 0 $$

$$ \lim_{y\rightarrow 0} \tau_t = \overline{\tau}_w $$

 - Computing flow in this manner is called the **method of wall functions**
 - Effects of viscosity neglected in the outer turbulent layer, so effects of viscosity only come through wall functions
 - If entire boundary layer is computed, including viscous wall layer, then the boundary conditions are the no-slip conditions:

$$ \overline{u} = \overline{v} = \tau_t = 0 \text{ at } y=0 $$

 - This is called a **low Reynolds number model**

## Algebraic Models

 - Eddy viscosity model:

$$ \tau_t = \rho \nu_t \frac{\partial\overline{u}}{\partial y} $$

 - Mixing length model:

$$ \tau_t = \rho \ell^2 \left| \frac{\partial\overline{u}}{\partial y} \right| \frac{\partial\overline{u}}{\partial y} $$

 - The two are related by:

$$ \nu_t = \ell^2 \left| \frac{\partial\overline{u}}{\partial y} \right| $$

### Cebeci-Smith Model:

$$ \nu_t = \kappa^2 y^2 \left| \frac{\partial\overline{u}}{\partial y} \right|
 \text{ for } 0<y<y_c
$$

$$ \nu_t = \alpha U(x) \delta_1(x) \gamma(x,y) $$

 - $y_c$ is the cross-over point of the two viscosity functions which is closest to the wall
 - $\alpha\approx 0.016$ is a constant
 - $U(x)$ is velocity at edge of boundary layer
 - $\delta_1(x)$ is the displacement thickness
 - $\gamma(x,y)$ is an intermittency function

### Michel et al. Model

$$ \frac{\ell}{\delta} = \lambda\tanh\left(\frac{\kappa y}{\lambda \delta}\right) $$

 - $\lambda\approx 0.085$ is a constant
 - $\delta$ is boundary layer thickness
 - For $y>0.6\delta$, mixing length is practically independent of $y$

### Summary

 - There are boundary layers where relation between $\nu_t$ or $\ell$ is exact (equilibrium boundary layers)
 - In all other cases, algebraic models are only an approximation

## One-Equation Models (Turbulent Kinetic Energy)

 - All non-algebraic turbulence models start with equation for kinetic energy of fluctuations
 - Turbulent diffusion commonly modelled by gradient-diffusion hypothesis:

$$ \overline{v^\prime \left(p^\prime + \frac{\rho}{2}q^2 \right)}
 = - \frac{\rho \nu_t}{Pr_k} \frac{\partial k}{\partial y}
$$

 - $Pr_k$ is a turbulent Prandtl number, commonly taken as $Pr_k=1.0$
 - Model for $k$ in fully turbulent region of steady boundary layer where viscous diffusion is negligible in comparison to turbulent diffusion:

$$ \overline{u} \frac{\partial k}{\partial x}
 + \overline{v} \frac{\partial k}{\partial y}
 = \frac{\partial}{\partial y} \left(\frac{\nu_t}{Pr_k}\frac{\partial k}{\partial y}\right)
 + \frac{\tau_t}{\rho}\frac{\partial\overline{u}}{\partial y}
 - \varepsilon
$$

 - Where $\varepsilon$ is the dissipation
 - To close the equation, assume $\nu_t = f(k, \varepsilon)$, and dimensional analysis gives:

$$ \nu_t = c_\mu \frac{k^2}{\varepsilon} $$

 - $c_\mu\approx 0.09$

 - Dimensional considerations allow a turbulence length scale, $L$, to be obtained:

$$ \nu_t = c_P L \sqrt{k} $$

 - $c_P\approx 0.55$ (subscript $P$ is because formula is due to Prandtl)
 - Combining the two equations above:

$$ L = c_\varepsilon \frac{k^{3/2}}{\varepsilon} $$

 - $c_\varepsilon=c_\mu/c_P\approx 0.168$

### Turbulence Model of Bradshaw et al.

 - Assume $\tau_t = a\rho k$, where $a\approx 0.3$
 - Resulting equation for $\tau_t$ is:

$$ \overline{u} \frac{\partial}{\partial x}\left(\frac{\tau_t}{a\rho}\right)
 + \overline{v} \frac{\partial}{\partial y}\left(\frac{\tau_t}{a\rho}\right)
 = - \left(\frac{\tau_{t,max}}{\rho}\right)^{1/2}
 \frac{\partial}{\partial y} \left(G \frac{\tau_t}{\rho}\right)
 + \frac{\tau_t}{\rho}\frac{\partial\overline{u}}{\partial y}
 - \frac{(\tau_t/\rho)^{3/2}}{L}
$$

 - Turbulence length scale is assumed to be function of $y/\delta$

$$ L = \delta f_1(y/\delta) $$

 - Turbulent diffusion proportional to $(\tau_{t,max}/\rho)^{1/2}$ where $\tau_{t,max}$ is the maximum value of $\tau_t$ in the region $0.25\delta<y<\delta$
 - Function $G$ is:

$$ G = (\tau_{t,max}/\rho U^2)^{1/2}f_2(y,\delta) $$

## Two Equation Models

### $k-\varepsilon$ Model

 - $k-\varepsilon$ adds another PDE to model dissipation, which is included into equation for $k$

$$ \overline{u}\frac{\partial\varepsilon}{\partial x}
 + \overline{v}\frac{\partial\varepsilon}{\partial y}
 = \frac{\partial}{\partial y}
 \left(\frac{\nu_t}{Pr_\varepsilon}\frac{\partial\varepsilon}{\partial y}\right)
 + c_{\varepsilon 1}\frac{\varepsilon}{k}\frac{\tau_t}{\rho}
 \frac{\partial\overline{u}}{\partial y}
 - c_{\varepsilon 2}\frac{\varepsilon^2}{k}
$$

 - Model constants are:

$$ c_{\varepsilon 1} = 1.44 $$
$$ c_{\varepsilon 2} = 1.87 $$
$$ Pr_\varepsilon = 1.3 $$

 - Also use equation:

$$ \nu_t = c_\mu\frac{k^2}{\varepsilon} $$

 - To match log law, must have:

$$ Pr_\varepsilon\sqrt{c_\mu}(c_{\varepsilon 2}-c_{\varepsilon 1}) = \kappa^2 $$

 - Model constants must be determined empirically
 - Production and dissipation terms have same structure as in $k$ equation, just multiplied by $\varepsilon/k$ and a constant
 - Recall $k$ equation:

$$ \overline{u} \frac{\partial k}{\partial x}
 + \overline{v} \frac{\partial k}{\partial y}
 = \frac{\partial}{\partial y} \left(\frac{\nu_t}{Pr_k}\frac{\partial k}{\partial y}\right)
 + \frac{\tau_t}{\rho}\frac{\partial\overline{u}}{\partial y}
 - \varepsilon
$$

 - The $k$ and $\varepsilon$ equations form a closed system that can be solved and the estimate of $\nu_t$ can be used in RANS momentum equations
 - Matching conditions:

$$ \lim_{y\rightarrow 0}\overline{u} = \frac{1}{\kappa}\ln y^+ + C^+ $$
$$ \lim_{y\rightarrow 0}\overline{v} = 0 $$
$$ \lim_{y\rightarrow 0}\tau_t = \overline{\tau}_w $$
$$ \lim_{y\rightarrow 0}\nu_t = \kappa y u_\tau $$
$$ \lim_{y\rightarrow 0}k = u_\tau^2/\sqrt{c_\mu} $$
$$ \lim_{y\rightarrow 0}\varepsilon = u_\tau^3/\kappa y $$

 - There is a singularity at $y=0$
 - When solving numerically, must consider wall at small distance from $y=0$
